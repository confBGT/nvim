local keymap = vim.keymap.set
local opts = { noremap = true, silent = true }

vim.g.mapleader = " "

keymap("n", "<leader>pv", vim.cmd.Ex)

keymap("n", "<C-d>", "<C-d>zz")
keymap("n", "<C-u>", "<C-u>zz")

keymap("n", "<C-j>", "<C-W>j")
keymap("n", "<C-k>", "<C-W>k")
keymap("n", "<C-h>", "<C-W>h")
keymap("n", "<C-l>", "<C-W>l")

keymap("n", "<C-Up>", "<cmd>resize -2<CR>")
keymap("n", "<C-Down>", "<cmd>resize +2<CR>")
keymap("n", "<C-Left>", "<cmd>vertical resize -2<CR>")
keymap("n", "<C-Right>", "<cmd>vertical resize +2<CR>")

keymap("n", "gh", "^")
keymap("n", "gm", "gM")
keymap("n", "gl", "$")

keymap("n", "<a-j>", "<cmd>m .+1<CR>==")
keymap("n", "<a-k>", "<cmd>m .-2<CR>==")

keymap("x", "<a-j>", "<cmd>move '>+1<CR>gv-gv")
keymap("x", "<a-k>", "<cmd>move '<-2<CR>gv-gv")

keymap("n", "Q", "<C-6>")
