return require("packer").startup(function(use)

    use { "wbthomason/packer.nvim" } -- Have Packer manage itself

    -- Dependencies
    use { "nvim-lua/plenary.nvim",  -- Neovim lua library
        opt = true,
        module = "plenary" }

    -- Colorschemes
    use { "shaunsingh/nord.nvim" }
    use { "lunarvim/templeos.nvim" }
    use { "aktersnurra/no-clown-fiesta.nvim" }
    use { "nyoom-engineering/oxocarbon.nvim" }
    use { "catppuccin/nvim", as = "catppuccin" }
    use { "rose-pine/neovim", as = "rose-pine" }
    use { "mountain-theme/vim", as = "mountain", branch = "master" }
    use { "alessandroyorba/alduin"}

    -- UI
    use { "lukas-reineke/indent-blankline.nvim",
        opt = true,
        event = "BufReadPost",
        config = function() require("modules.ui.indent_blankline") end, }

    use { "goolord/alpha-nvim",
        opt = true,
        event = "BufWinEnter",
        config = function() require("modules.ui.alpha") end, }

    -- Completion
    use { "hrsh7th/nvim-cmp",   -- Completion engine
        after = "LuaSnip",
        requires = {
            { "hrsh7th/cmp-path", opt = true, after = "nvim-cmp" }, -- Path completion source
            { "hrsh7th/cmp-buffer", opt = true, after = "nvim-cmp" },   -- Buffer completion source
            { "hrsh7th/cmp-nvim-lsp", opt = true, after = "nvim-cmp" }, -- LSP completion source
            { "saadparwaiz1/cmp_luasnip", after = "nvim-cmp" },  -- LuaSnip completion source
        },
        config = function() require("modules.completion.cmp") end, }

    use { "L3MON4D3/LuaSnip",   -- Snippet engine
        event = "InsertEnter" }

    use { "windwp/nvim-autopairs", -- Autopairs
        opt = true,
        after = "nvim-cmp",
        config = function() require("modules.completion.autopairs") end, }

    -- LSP
    use { "williamboman/mason.nvim", -- Portable package manager
        opt = false,
        requires = "williamboman/mason-lspconfig.nvim" }

    use { "neovim/nvim-lspconfig",  -- LSP client configs
        opt = true,
        event = "BufReadPre",
        after = "mason.nvim",
        config = function() require("modules.lsp") end, }

    -- Editor
    use { "nvim-treesitter/nvim-treesitter", -- Treesitter syntax highlighting
        opt = true,
        event = "BufReadPost",
        config = function() require("modules.editor.treesitter") end, }

    use { "numToStr/Comment.nvim",  -- Comment plugin
        opt = true,
        event = "BufReadPost",
        config = function() require("modules.editor.comment") end, }

    use { "Pocco81/true-zen.nvim", -- Distraction-free writing for Neovim
        opt = true,
        cmd = {
            "TZAtaraxis",
            "TZMinimalis",
            "TZNarrow",
            "TZFocus",
        },
        config = function() require("modules.editor.true-zen") end, }

    -- Tools
    use { "nvim-telescope/telescope.nvim",  -- Extendable fuzzy finder
        opt = true,
        cmd = "Telescope",
        event = "BufReadPost",
        module = "Telescope",
        requires = {
            { "nvim-lua/plenary.nvim" },
        },
        config = function() require("modules.tools.telescope") end, }

end)
