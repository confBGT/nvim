local set = vim.opt

vim.g.transparent = 0           -- Transparent background
vim.g.netrw_banner = false      -- Suppress the Netrw banner

set.title = true
set.titlestring = "nvim | %f"

set.number = true               -- Precede each line with its line number
set.relativenumber = true       -- Show the line number relative to the cursor

set.tabstop = 4                 -- Number of spaces a <Tab> counts for
set.shiftwidth = 4              -- Number of spaces to use for (auto)indent
set.softtabstop = 4             -- Number of spaces a <Tab> counts for while performing editing operations
set.expandtab = true            -- Use appropiate number of spaces to insert a <Tab> in Insert mode

set.signcolumn = "yes"          -- Always draw the signcolumn

set.wrap = false                -- Don't wrap lines

set.cursorline = true           -- Highlight the text line of the cursor

set.scrolloff = 4               -- Number of lines to keep above and below the cursor
set.sidescrolloff = 4           -- Number of columns to keep to the left and to the right of the cursor
