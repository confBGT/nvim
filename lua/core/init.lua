require("core.options")
require("core.keymaps")
require("keymaps")

local async
async =
    vim.loop.new_async(
    vim.schedule_wrap(
        function()
            require("core.packer")
            require("core.color")
            async:close()
        end
    )
)

async:send()

