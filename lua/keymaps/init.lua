local keymap = vim.keymap.set
local opts = { noremap = true, silent = true }

-- Telescope
keymap("n", "<leader>sf", "<cmd>Telescope find_files<CR>", opts)
keymap("n", "<leader>fw", "<cmd>Telescope live_grep<CR>", opts)
keymap("n", "<leader>fo", "<cmd>Telescope oldfiles<CR>", opts)
keymap("n", "<leader>fb", "<cmd>Telescope buffers<CR>", opts)
keymap("n", "<leader>ft", "<cmd>Telescope colorscheme<CR>", opts)
keymap("n", "<C-p>", "<cmd>Telescope find_files<CR>", opts)

keymap("n", "<leader>za", "<cmd>TZAtaraxis<CR>", opts)

keymap("n", "<leader>gw", '<cmd>set tw=9999<CR>gggqGgg"+yG<cmd>set tw=80<CR>gggwG', opts)
