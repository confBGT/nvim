local cmp = require("cmp")
local icons = require("modules.ui.icons").get("kind")

cmp.setup({
    formatting = {
        fields = {"kind", "abbr", "menu"},
        format = function(entry, vim_item)
            vim_item.kind = icons[vim_item.kind]
            vim_item.menu = ({
                nvim_lsp = "[LSP]",
                luasnip = "[SNP]",
                buffer = "[BUF]",
                path = "[PTH]",
            }) [entry.source.name]
            return vim_item
        end
    },
    window = {
        completion = cmp.config.window.bordered(),
        documentation = cmp.config.window.bordered(),
    },
    completion = {
        completeopt = "menu,menuone,noinsert",
    },
    snippet = {
        expand = function(args)
            require("luasnip").lsp_expand(args.body)
        end,
    },
    mapping = cmp.mapping.preset.insert({
        ["<C-j>"] = cmp.mapping.select_next_item(),
        ["<C-k>"] = cmp.mapping.select_prev_item(),
        ["<Tab>"] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.confirm({ select = true })
            elseif require("luasnip").jumpable(1) then
                require("luasnip").jump(1)
                -- ls.jump(1)
            else
                fallback()
            end
        end, { "i", "s" }),
    }),
        ["<S-Tab>"] = cmp.mapping(function(fallback)
            if require("luasnip").jumpable(-1) then
                require("luasnip").jump(-1)
            -- if ls.jumpable(-1) then
            --     ls.jump(-1)
            else
                fallback()
            end
        end, { "i", "s" }),
    sources = {
        { name = "nvim_lsp" },
        { name = "luasnip" },
        { name = "buffer" },
        { name = "path" },
    },
})
