require("nvim-autopairs").setup({})

local cmp = require("cmp")

cmp.event:on(
    "confirm_done",
    require("nvim-autopairs.completion.cmp").on_confirm_done({
        filetypes = {
            ["*"] = {
                ["("] = {
                    kind = {
                        cmp.lsp.CompletionItemKind.Function,
                        cmp.lsp.CompletionItemKind.Method,
                    },
                    handler = require("nvim-autopairs.completion.handlers")["*"],
                }
            }
        }
    })
)
