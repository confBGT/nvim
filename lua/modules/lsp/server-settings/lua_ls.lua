return {
    Lua = {
        diagnostics = {
            globals = { "vim" },
        },
        semantic = {
            enable = false,
        },
    },
}
