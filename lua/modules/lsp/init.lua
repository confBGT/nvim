vim.api.nvim_command([[packadd cmp-nvim-lsp]])

local mason = require("mason")
local mason_lspconfig = require("mason-lspconfig")
local lspconfig = require("lspconfig")

local servers = {
    "lua_ls",
}

mason.setup({})
mason_lspconfig.setup({
    ensure_installed = servers,
    automatic_installation = false,
})

local capabilities = require("cmp_nvim_lsp").default_capabilities(vim.lsp.protocol.make_client_capabilities())

mason_lspconfig.setup_handlers({
    function(server)
        local server_settings = require("modules.lsp.server-settings." .. server)
        lspconfig[server].setup({
            capabilities = capabilities,
            settings = server_settings,
        })
    end,
})
