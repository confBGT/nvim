local groups = {
    "Normal",
    "NormalNC",
    "NormalFloat",
    "SignColumn",
}

local function TransparencyEnable()
    for _, v in ipairs(groups) do
        local val = vim.tbl_extend(
            "force",
            vim.api.nvim_get_hl_by_name(v, true),
            { bg = "NONE", ctermbg = "NONE" }
        )
        vim.api.nvim_set_hl(0, v, val)
    end
end

local group = vim.api.nvim_create_augroup("Transparency", { clear = true })

vim.api.nvim_create_autocmd("ColorScheme", {
    callback = function()
        if vim.g.transparent == 1 then
            TransparencyEnable()
        end
    end,
    group = group
})

if vim.g.transparent == 1 then
    TransparencyEnable()
end
