vim.keymap.set("n", "<leader>lp",
    function()
        vim.cmd('silent exec "!groff -ms -K utf8 -T pdf % > /tmp/usr-nvimgroff.pdf"')
        vim.cmd('silent exec "!setsid -f zathura /tmp/usr-nvimgroff.pdf"')
    end)

vim.opt.colorcolumn = { 81 }
vim.opt.tw = 80

-- vim.api.nvim_create_autocmd("BufWritePost", {
--     group = vim.api.nvim_create_augroup("PazGroffWrite", { clear = true }),
--     callback = function()
--         vim.cmd('silent exec "!groff -ms -K utf8 -T pdf % > /tmp/usr-nvimgroff.pdf"')
--     end
-- })
