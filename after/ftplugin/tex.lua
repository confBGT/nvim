vim.opt.tw = 80
vim.opt.colorcolumn = { 81 }

vim.keymap.set("n", "g<C-g>", "<cmd>!texcount -incbib %<CR>")
vim.keymap.set("n", "<leader>ll", "<cmd>!pcc %<CR>")

vim.g.tex_indent_items = 0
vim.g.tex_indent_and = 0
vim.g.tex_indent_brace = 0
